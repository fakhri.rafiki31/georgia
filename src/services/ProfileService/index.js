import { Op } from 'sequelize'
import { Room, sequelize } from '../../models'

class ProfileService {
  static getTotalAppearances = async (userName) => {
    const result = await Room.findOne(
      {
        attributes: [[sequelize.fn('count', sequelize.col('id')), 'appearance']],
        where:
        {
          [Op.or]:
          [{ firstPlayer: userName }, { secondPlayer: userName }],
        },
      },
    )

    const { appearance } = result.dataValues

    return appearance
  }

  static getTotalWins = async (userName) => {
    const result = await Room.findOne(
      {
        attributes: [[sequelize.fn('count', sequelize.col('id')), 'win']],
        where:
        {
          result:
            {
              [Op.iLike]: `${userName} WIN%`,
            },
        },
      },
    )

    const { win } = result.dataValues

    return win
  }

  static getTotalChoiceUsed = async (choice, userName) => {
    const result = await Room.findOne(
      {
        attributes: [[sequelize.fn('count', sequelize.col('id')), 'total']],
        where:
        {
          [Op.or]:
          [
            {
              [Op.and]:
                [{ firstPlayer: userName }, { firstPlayerChoice: choice }],
            },
            {
              [Op.and]:
                [{ secondPlayer: userName }, { secondPlayerChoice: choice }],
            },
          ],
        },
      },
    )

    const { total } = result.dataValues

    return total
  }

  static getStatistic = async (userName) => {
    const appearance = await this.getTotalAppearances(userName)
    if (appearance === 0) {
      return {
        appearance: 0, winRate: 0, rockRate: 0, paperRate: 0, scissorsRate: 0,
      }
    }

    const win = await this.getTotalWins(userName)
    const winRate = ((win / appearance) * 100).toFixed(0)
    const totalRock = await this.getTotalChoiceUsed('rock', userName)
    const rockRate = ((totalRock / appearance) * 100).toFixed(0)
    const totalPaper = await this.getTotalChoiceUsed('paper', userName)
    const paperRate = ((totalPaper / appearance) * 100).toFixed(0)
    const totalScissors = await this.getTotalChoiceUsed('scissors', userName)
    const scissorsRate = ((totalScissors / appearance) * 100).toFixed(0)

    return {
      appearance, winRate, rockRate, paperRate, scissorsRate,
    }
  }
}

export default ProfileService
