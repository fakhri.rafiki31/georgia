import jwt from 'jsonwebtoken'

require('dotenv').config()

class UserService {
  static DEFAULT_OPTIONS = {
    algorithm: 'HS256',
    expiresIn: 3600,
  }

  static sign = (payload, success, error = () => {}) => jwt.sign(
    payload,
    process.env.JWT_SECRET,
    this.DEFAULT_OPTIONS,
    (e, encoded) => {
      if (e) return error(e)

      return success(encoded)
    },
  )

  static verify = (token, success, error = () => {}) => jwt.verify(
    token,
    process.env.JWT_SECRET,
    (e, decoded) => {
      if (e) return error()

      return success(decoded)
    },
  )
}

export default UserService
