import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      const { UserProfile, Room } = models

      User.hasOne(UserProfile, { foreignKey: 'id', as: 'profile' })
      User.belongsToMany(
        Room,
        { through: 'UserRoom', foreignKey: 'firstPlayer' },
      )
      User.belongsToMany(
        Room,
        { through: 'UserRoom', foreignKey: 'secondPlayer' },
      )
    }
  }

  User.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    sequelize,
    modelName: 'User',
    timestamps: true,
    paranoid: true,
    createdAt: false,
    updatedAt: false,
  })

  return User
}
