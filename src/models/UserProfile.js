import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class UserProfile extends Model {
    static associate(models) {
      const { User } = models

      UserProfile.belongsTo(User, { foreignKey: 'id' })
    }
  }

  UserProfile.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  }, {
    sequelize,
    modelName: 'UserProfile',
  })

  return UserProfile
}
