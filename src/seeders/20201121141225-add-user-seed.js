const bcrypt = require('bcrypt')

const salt = bcrypt.genSaltSync()
const password = bcrypt.hashSync('password', salt)

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('User', [
      {
        id: 'c4cb353b-0f1e-455b-b634-562a6d39bc90',
        userName: 'alpha',
        password,
        isAdmin: true,
      },
      {
        id: '61dfc472-f3c2-4c99-a40f-b190cef9a8b6',
        userName: 'beta',
        password,
        isAdmin: false,
      },
      {
        id: '17d8679f-b7ad-429b-9a87-d8a5925c4e32',
        userName: 'charlie',
        password,
        isAdmin: false,
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User', null, {});
  },
}
