module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserProfile', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        onDelete: 'cascade',
        references: {
          model: 'User',
          key: 'id',
        },
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserProfile');
  },
}
