import express from 'express'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import path from 'path'
import yaml from 'yamljs'
import swaggerUi from 'swagger-ui-express'
import apis from './routes/apis'
import views from './routes/views'

require('dotenv').config()

const app = express()
app.use(cookieParser())
app.use(express.static('public'))
app.use(express.json())
app.use(cors())

app.set('view engine', 'ejs')
app.set('views', 'src/views')

app.use('/', views)
app.use('/apis', apis)

const swaggerDoc = yaml.load(path.resolve(__dirname, '../openapi.yaml'))
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc))

app.get('*', (req, res) => res.status(404).send('404 Not Found'))

app.listen(3004, () => console.log('running'))
