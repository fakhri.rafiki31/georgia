import { validationResult } from 'express-validator'
import AuthMiddleware from './Auth'

class Middleware {
  static Guest = (req, res, next) => this.handler('guest', req, res, next)

  static Auth = (req, res, next) => this.handler('auth', req, res, next)

  static Admin = (req, res, next) => this.handler('admin', req, res, next)

  static messageBuilder = async (errors = []) => {
    const message = []

    errors.forEach((e) => {
      message.push(`${e.msg} for ${e.param} `)
    })

    return message
  }

  static handler = async (type, req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(400).json({ message: await this.messageBuilder(errors.array()) })
    }

    switch (type) {
      case 'guest': return AuthMiddleware.hasLoggedIn(req, res, next)
      case 'auth': return AuthMiddleware.isAuth(req, res, next)
      case 'admin': return AuthMiddleware.isAdmin(req, res, next)
      default: return res.status(403)
    }
  }
}

export default Middleware
