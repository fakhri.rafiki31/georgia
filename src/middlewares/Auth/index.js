import UserService from '../../services/UserService'

class AuthMiddleware {
  static STATUS_CODE = {
    401: 'Unauthorized',
    403: 'Forbidden',
  }

  static handleAuthErrorRes = (req, res, statusCode = 401) => {
    const isRespPage = req.headers.accept.includes('html')

    if (isRespPage) {
      return res.redirect('/')
    }

    return res.status(statusCode).json({ message: this.STATUS_CODE[statusCode] })
  }

  static getTokenFromCookies = (req) => {
    const { cookies: { token = null } } = req

    return token
  }

  static getTokenFromHeader = (req) => {
    if (req.headers.authorization) {
      return req.headers.authorization.split(' ')[1]
    }

    console.log(req.headers.authorization)

    return null
  }

  static hasLoggedIn = (req, res, next) => {
    const token = this.getTokenFromCookies(req)

    if (!token) return next()

    UserService.verify(
      token,
      (decoded) => {
        req.decoded = decoded
      },
    )

    return next()
  }

  static isAuth = (req, res, next) => {
    let token = this.getTokenFromHeader(req) // this.getTokenFromHeader(req)

    if (!token) {
      token = this.getTokenFromCookies(req)
    }

    if (!token) return this.handleAuthErrorRes(req, res)

    return UserService.verify(
      token,
      (decoded) => {
        req.decoded = decoded

        return next()
      },
      () => this.handleAuthErrorRes(req, res),
    )
  }

  static isAdmin = (req, res, next) => {
    let token = this.getTokenFromHeader(req) // this.getTokenFromHeader(req)

    if (!token) {
      token = this.getTokenFromCookies(req)
    }

    if (!token) return this.handleAuthErrorRes(req, res)

    return UserService.verify(
      token,
      (decoded) => {
        if (decoded.isAdmin) {
          req.decoded = decoded

          return next()
        }

        return this.handleAuthErrorRes(req, res, 403)
      },
      () => this.handleAuthErrorRes(req, res),
    )
  }
}

export default AuthMiddleware
