import express from 'express';

import UserController from '../../controllers/views/UserController';
import GameController from '../../controllers/views/GameController';
import AdminController from '../../controllers/views/AdminController';
import Middleware from '../../middlewares';

const router = express.Router();

router.get('/admin', [Middleware.Admin], AdminController.getIndex);
router.get('/admin/users', [Middleware.Admin], AdminController.getIndexUser);
router.get(
  '/admin/histories',
  [Middleware.Admin],
  AdminController.getIndexHistories,
);

router.get('/', [Middleware.Guest], UserController.getIndex);
router.get('/profil', [Middleware.Auth], UserController.getProfil);
router.get('/profilstats', [Middleware.Auth], UserController.getProfilStats);

router.get('/game', [Middleware.Auth], GameController.getIndex);
router.get('/gameboard', [Middleware.Auth], GameController.getGameBoard);

export default router;
