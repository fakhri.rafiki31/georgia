import express from 'express'
import Middleware from '../../middlewares'
import UserValidator from '../../validators/UserValidator'
import UserController from '../../controllers/apis/UserController'
import UserProfileValidator from '../../validators/UserProfileValidator'
import UserProfileController from '../../controllers/apis/UserProfileController'
import RoomController from '../../controllers/apis/RoomController'
import RoomValidator from '../../validators/RoomValidator'
import GameController from '../../controllers/apis/GameController'
import GameValidator from '../../validators/GameValidator'

const router = express.Router()

router.post('/login', [UserValidator.login, Middleware.Guest], UserController.login)

router.get('/users', [Middleware.Admin], UserController.get)
router.post('/users', [UserValidator.create, Middleware.Guest], UserController.create)
router.delete('/users/:id', [UserValidator.delete, Middleware.Admin], UserController.delete)
router.patch('/users/:id', [UserValidator.update, Middleware.Admin], UserController.update)

router.get('/userprofiles', [Middleware.Auth], UserProfileController.getOne)
router.get('/userprofiles/statistic', [Middleware.Auth], UserProfileController.getStatistic)
router.patch('/userprofiles/:id', [UserProfileValidator.update, Middleware.Auth], UserProfileController.update)

router.get('/rooms', [Middleware.Auth], RoomController.get)
router.post('/rooms', [Middleware.Auth], RoomController.create)
router.patch('/rooms/:roomId', [RoomValidator.join, Middleware.Auth], RoomController.join)
router.delete('/rooms/:roomId', [RoomValidator.delete, Middleware.Admin], RoomController.delete)

router.get('/games/:roomId', [GameValidator.get, Middleware.Auth], GameController.get)
router.patch('/games/:roomId', [GameValidator.play, Middleware.Auth], GameController.play)

export default router
