import { Room, sequelize } from '../../../models'
import GameService from '../../../services/GameService'

class GameController {
  static get = (req, res) => {
    const { roomId } = req.params

    return Room.findOne({ where: { id: roomId } })
      .then(
        (room) => {
          if (!room) return res.status(400).json({ message: 'game does not exist' })

          return res.status(200).json({ room })
        },
      ).catch((e) => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static play = async (req, res) => {
    const tran = await sequelize.transaction()

    try {
      const { roomId } = req.params
      const { choice } = req.body
      const { userName } = req.decoded

      const room = await Room.findOne({ where: { id: roomId } })

      return GameService.validatePlayer(room, userName, choice)
        .then(async (player) => {
          let playerChoiceToSet = { firstPlayerChoice: choice }

          if (player === 'secondPlayer') {
            playerChoiceToSet = { secondPlayerChoice: choice }
          }

          return Room.update(
            playerChoiceToSet,
            {
              where: { id: roomId }, returning: true, plain: true, transaction: tran,
            },
          ).then(async (updatedRoom) => {
            const result = await GameService.getResult(updatedRoom[1])
            const gameResult = await Room.update(
              { result },
              {
                where: { id: roomId }, returning: true, plain: true, transaction: tran,
              },
            )

            res.status(200).json({ room: { ...gameResult[1].dataValues } })
            return tran.commit();
          }).catch(async (e) => {
            console.log(e);
            await tran.rollback();
            return res.status(500).json({ message: 'Internal Server Error' })
          })
        })
        .catch(async (e) => {
          console.log(e)
          await tran.rollback();
          return res.status(400).json({ message: e })
        })
    } catch (e) {
      console.log(e)
      await tran.rollback();
      return res.status(500).json({ message: 'Internal Server Error' })
    }
  }
}

export default GameController
