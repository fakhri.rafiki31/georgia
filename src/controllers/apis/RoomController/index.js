import { Op } from 'sequelize'
import { User, Room } from '../../../models'

class RoomController {
  static get = (req, res) => Room.findAll()
    .then((rooms) => res.status(200).json({ message: rooms }))
    .catch((e) => res.status(500).json({ message: 'Internal Server Error' }))

  static getById = (req, res) => {
    const { id } = req.params

    return Room.findOne({ where: { id } })
      .then((room) => res.status(200).json({ message: room }))
      .catch((e) => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static create = (req, res) => {
    const { userName } = req.decoded

    return Room.create({ firstPlayer: userName })
      .then((room) => res.status(201).json({ roomId: room.id }))
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static join = async (req, res) => {
    try {
      const { roomId } = req.params
      const { userName } = req.decoded

      /*
      * @todo:
      * 1. change this logic to service
      */

      const existRoom = await Room.findOne({
        where: { id: roomId },
      })

      if (!existRoom) {
        return res.status(400).json({ message: 'Room does not exist' })
      }

      const hasJoinRoom = await Room.findOne({
        where: {
          [Op.and]: [
            { id: roomId },
            { [Op.or]: [{ firstPlayer: userName }, { secondPlayer: userName }] },
          ],
        },
      })

      if (hasJoinRoom) {
        return res.status(200).json({ roomId })
      }

      const availRoom = await Room.findOne({
        where: {
          [Op.and]: [
            { id: roomId },
            { secondPlayer: { [Op.is]: null } },
          ],
        },
      })

      if (!availRoom) {
        return res.status(400).json({ message: 'Room is full' })
      }

      return Room.findOne({
        where: { id: roomId },
      }).then(
        (room) => {
          room.update({ secondPlayer: userName })
            .then(() => res.status(200).json({ roomId: room.id }))
        },
      ).catch((e) => res.status(500).json({ message: 'Internal Server Error' }))
    } catch (e) {
      console.log(e)

      return res.status(500).json({ message: 'Internal Server Error' })
    }
  }

  static delete = async (req, res) => {
    const { roomId: id } = req.params

    return Room.destroy({ where: { id } })
      .then((user) => {
        if (user) return res.status(200).json({ message: 'succesfully deleted' })

        return res.status(400).json({ message: 'not found' })
      })
      .catch((e) => {
        console.log(e)
        return res.status(500).json({ message: 'Internal Server Error' })
      })
  }
}

export default RoomController
