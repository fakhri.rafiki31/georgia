import bcrypt from 'bcrypt'
import UserService from '../../../services/UserService'
import { User, UserProfile, sequelize } from '../../../models'

class UserController {
  static COOKIES_OPTIONS = {
    httpOnly: true,
    maxAge: 3600,
  }

  static login = (req, res) => {
    const { userName, password } = req.body

    return User.findOne({
      where: { userName },
    }).then(
      (user) => {
        if (user && bcrypt.compareSync(password, user.password)) {
          const { id, isAdmin } = user

          return UserService.sign(
            { id, userName, isAdmin },
            (token) => {
              res.cookie('token', token, { maxAge: 1000 * 60 * 10 })
              res.status(200).json({ token })
            },
            (e) => res.status(401).json({ message: 'Unauthorized' }),
          )
        }

        return res.status(401).json({ message: 'Unauthorized' })
      },
    ).catch(
      (e) => res.status(500).json({ message: 'Internal Server Error' }),
    )
  }

  static get = (req, res) => User.findAll({
    include: [{
      model: UserProfile,
      as: 'profile',
      attributes: ['name', 'email'],
    }],
    attributes: ['id', 'userName', 'isAdmin'],
  }).then(
    (users) => res.status(200).json({ message: users }),
  )

  static create = async (req, res) => {
    try {
      const {
        userName, email, name, password,
      } = req.body
      const salt = bcrypt.genSaltSync()

      const user = await User.findOne({ where: { userName } })

      if (user) {
        return res.status(400).json({ message: 'userName is already exist' })
      }

      const userProfile = await UserProfile.findOne({ where: { email } })

      if (userProfile) {
        return res.status(400).json({ message: 'email is already exist' })
      }

      const result = await sequelize.transaction(
        (tran) => User.create(
          { userName, password: bcrypt.hashSync(password, salt) },
          { transaction: tran },
        ).then(
          (createdUser) => UserProfile.create(
            { id: createdUser.id, name, email },
            { transaction: tran },
          ).then(
            async () => {
              UserService.sign(
                { id: createdUser.id, userName, isAdmin: false },
                (token) => {
                  res.cookie('token', token, { maxAge: 1000 * 60 * 10 })
                  res.status(200).json({ token })
                },
                (e) => res.status(401).json({ message: 'Unauthorized' }),
              )
            },
          ),
        ),
      )

      return result
    } catch (e) {
      console.log(e)
      return res.status(500).json({ message: 'Internal Server Error' })
    }
  }

  static update = async (req, res) => {
    const { id } = req.params
    const { isAdmin } = req.body

    return User.findOne({ where: { id } })
      .then((user) => {
        if (!user) return res.status(400).json({ message: 'Not Found' })

        return user.update({ isAdmin })
          .then(() => res.status(200).json({ message: 'successfully updated' }))
      })
      .catch((e) => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static delete = (req, res) => {
    const { id } = req.params

    return User.destroy({ where: { id } })
      .then((user) => {
        if (user) return res.status(200).json({ message: 'succesfully deleted' })

        return res.status(400).json({ message: 'not found' })
      })
      .catch((e) => {
        console.log(e)
        return res.status(500).json({ message: 'Internal Server Error' })
      })
  }
}

export default UserController
