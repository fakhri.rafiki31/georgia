import { Op } from 'sequelize'
import { UserProfile, User, sequelize } from '../../../models'
import ProfileService from '../../../services/ProfileService'

class UserProfileController {
  static update = async (req, res) => {
    try {
      const { id } = req.params
      const { name, email } = req.body

      const profile = await UserProfile.findOne({ where: { id } })

      if (!profile) return res.status(400).json({ message: 'not found' })

      const profileByEmail = await UserProfile.findOne({
        where: {
          [Op.and]: [{ email }, { id: { [Op.ne]: id } }],
        },
      })

      if (email && profileByEmail) {
        return res.status(400).json({ message: 'email is already exist' })
      }

      await profile.update({ name, email })

      return res.status(200).json({ message: 'successfully updated' })
    } catch (e) {
      console.log(e)
      return res.status(500).json({ message: 'Internal Server Error' })
    }
  }

  static getOne = (req, res) => {
    const { userName } = req.decoded

    return User.findOne({ where: { userName } })
      .then((user) => UserProfile.findOne({ where: { id: user.id } })
        .then((profile) => res.status(200).json({ profile })))
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static getStatistic = async (req, res) => {
    try {
      const { userName } = req.decoded

      const statistic = await ProfileService.getStatistic(userName)

      return res.json(statistic)
    } catch (e) {
      console.log(e)
      return res.status(500).json({ message: 'Internal Server Error' })
    }
  }
}

export default UserProfileController
