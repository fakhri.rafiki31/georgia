class AdminController {
  static getIndex = (req, res) => {
    res.render('admin');
  };

  static getIndexUser = (req, res) => {
    res.render('adminManageUser');
  };

  static getIndexHistories = (req, res) => {
    res.render('adminHistories');
  };
}

export default AdminController;
