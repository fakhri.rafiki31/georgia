class GameController {
  static getIndex = (req, res) => {
    const { decoded: user } = req;

    res.render('game', { user });
  };

  static getGameBoard = (req, res) => {
    const { decoded: user } = req;

    res.render('gameBoardPage', { user });
  };
}

export default GameController;
