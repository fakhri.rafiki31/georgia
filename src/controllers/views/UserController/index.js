import ProfileService from '../../../services/ProfileService'

class LoginController {
  static getIndex = (req, res) => {
    const { decoded: user } = req

    res.render('index', { user })
  }

  static getProfil = (req, res) => {
    const { decoded: user } = req

    res.render('editProfil', { user })
  }

  static getProfilStats = async (req, res) => {
    try {
      const { decoded: user } = req

      const stats = await ProfileService.getStatistic(user.userName)

      console.log({ user, stats })

      res.render('userProfilStatistic', { user, stats })
    } catch (error) {
      res.send('500 Internal Server Error')
    }
  }
}

export default LoginController
