import { body, param } from 'express-validator'

class UserValidator {
  static login = [
    body('userName').exists().notEmpty().isString(),
    body('password').exists().notEmpty().isString(),
  ]

  static create = [
    body('userName').exists().notEmpty().isString(),
    body('name').exists().notEmpty().isString(),
    body('email').exists().isEmail(),
    body('password').exists().notEmpty().isString(),
  ]

  static update = [
    param('id').exists().notEmpty().isUUID(),
    body('isAdmin').exists().notEmpty().toBoolean()
      .isBoolean(),
  ]

  static delete = param('id').exists().isUUID()
}

export default UserValidator
