import { body, param } from 'express-validator'

class UserProfileValidator {
  static update = [
    param('id').exists().isUUID(),
    body('name').optional().isString(),
    body('email').optional().isEmail(),
  ]
}

export default UserProfileValidator
