import { param, body } from 'express-validator'

class GameValidator {
  static play = [
    param('roomId').exists().isUUID(),
    body('choice').exists().notEmpty().isString(),
  ]

  static get = param('roomId').exists().isUUID()
}

export default GameValidator
