import { body, param } from 'express-validator'

class UserProfileValidator {
  static join = param('roomId').exists().notEmpty().isUUID()

  static delete = param('roomId').exists().notEmpty().isUUID()
}

export default UserProfileValidator
