import { body } from 'express-validator'

class LoginValidator {
  static create = () => [body('userName').notEmpty().isString(), body('password').notEmpty().isString()]
}

export default LoginValidator
