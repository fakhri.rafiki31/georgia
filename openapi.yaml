openapi: 3.0.1

info:
  title: Swagger Documentation
  description: Team Georgia
  version: 0.1.0
  
servers:
- url: http://localhost:3004/apis

paths:
  /users:
    get:
      tags:
        - users
      summary: Get all users
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: string
                          format: uuid
                        userName:
                          type: string
                          example: alpha
                        profile:
                          type: object
                          properties:
                            name:
                              type: string
                              example: alpha beta
                            email:
                              type: string
                              format: email
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
        403:
          description: Forbidden (Admin Only)
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Forbidden
    post:
      tags:
        - users
      summary: Create user and autologin
      requestBody:
        description: user information
        content:
          application/json:
            schema:
              type: object
              properties:
                userName:
                  type: string
                  example: badrun
                password:
                  type: string
                  example: password
                name:
                  type: string
                  example: badrun saja
                email:
                  type: string
                  example: badrun@domain.com
      responses:
        201:
          description: created
          content:
            application/json:
              schema:
                type: object
                properties:
                  token:
                    type: string
                    example: token
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: userName is already exist
  
  /users/{id}:
    parameters:
      - name: id
        in: path
        description: user id
        required: true
        schema:
          type: string
          format: uuid
    delete:
      tags:
        - users
      summary: delete user by id
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: successfully deleted
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: array
                    items:
                      type: string
                      example: Invalid value for id
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
        403:
          description: Forbidden (Admin Only)
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Forbidden
                    
    patch:
      tags:
        - users
      summary: update user information
      security:
        - bearerAuth: []
      parameters:
        - name: id
          in: path
          description: user id
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: update user information
        content:
          application/json:
            schema:
              type: object
              properties:
                idAdmin:
                  type: boolean
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: successfully updated
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: array
                    items:
                      type: string
                      example: Invalid value for isAdmin
        403:
          description: Forbidden (Admin Only)
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Forbidden
  
  /userProfiles:
    get:
      tags:
        - profiles
      summary: get a user profile based on token
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  profile:
                    type: object
                    properties:
                      id:
                        type: string
                        format: uuid
                      name:
                        type: string
                        example: alpha adanya
                      email:
                        type: string
                        format: email
                        example: alpha@domain.com
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
      
  /userProfiles/{id}:
    parameters:
      - name: id
        in: path
        description: user id
        required: true
        schema:
          type: string
          format: uuid 
    patch:
      tags:
        - profiles
      summary: update profile
      security:
        - bearerAuth: []
      requestBody:
        description: update name or email
        content:
          application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
                  example: tidak alpha adanya
                email:
                  type: string
                  format: email
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: succesfully updated
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: array
                    items:
                      type: string
                      example: Invalid value for email
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized

  /userProfiles/statistic:
    get:
      tags:
        - profiles
      summary: get user statistic based on token
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  appearance:
                    type: string
                    example: 7
                  winRate:
                    type: string
                    example: 100
                  rockRate:
                    type: string
                    example: 100
                  paperRate:
                    type: string
                    example: 0
                  scissorsRate:
                    type: string
                    example: 0
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
                  
  /rooms:
    get:
      tags:
        - rooms
      summary: get all rooms
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: string
                          format: uuid
                        firstPlayer:
                          type: string
                          example: alpha
                        secondPlayer:
                          type: string
                          example: beta
                        firstPlayerChoice:
                          type: string
                          example: rock
                        secondPlayerChoice:
                          type: string
                          example: paper
                        result:
                          type: string
                          example: beta WIN
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
        403:
          description: Forbidden (Admin Only)
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Forbidden
    post:
      tags:
        - rooms
      summary: create room
      security:
        - bearerAuth: []
      responses:
        201:
          description: created
          content:
            application/json:
              schema:
                type: object
                properties:
                  roomId:
                    type: string
                    format: uuid
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
                    
  /rooms/{id}:
    parameters:
      - name: id
        in: path
        description: room id
        required: true
        schema:
          type: string
          format: uuid
    patch:
      tags:
        - rooms
      summary: Join the room using roomId
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  roomId:
                    type: string
                    format: uuid
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Room is full        
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized          
    delete:
      tags:
        - rooms
      summary: delete room
      security:
        - bearerAuth: []
      parameters:
        - name: id
          in: path
          description: user id
          required: true
          schema:
            type: string
            format: uuid
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: successfully deleted
        400:
          description: Room does not exists
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Not Found       
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
        403:
          description: Forbidden (Admin Only)
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Forbidden
                    
  /games/{roomId}:
    parameters:
      - name: roomId
        in: path
        description: room id
        required: true
        schema:
          type: string
          format: uuid
    get:
      tags:
        - games
      summary: get game information
      security:
        - bearerAuth: []
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  room:
                    type: object
                    properties:
                      id:
                        type: string
                        format: uuid
                      firstPlayer:
                        type: string
                        example: alpha
                      secondPlayer:
                        type: string
                        example: beta
                      firstPlayerChoice:
                        type: string
                        example: rock
                      secondPlayerChoice:
                        type: string
                        example: paper
                      result:
                        type: string
                        example: beta WIN
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Game does not exists        
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
    patch:
      tags:
        - games
      summary: playing game (call when choose an option)
      security:
        - bearerAuth: []
      requestBody:
        description: choose game option
        content:
          application/json:
            schema:
              type: object
              properties:
                choice:
                  type: string
                  example: rock
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  room:
                    type: object
                    properties:
                      id:
                        type: string
                        format: uuid
                      firstPlayer:
                        type: string
                        example: alpha
                      secondPlayer:
                        type: string
                        example: beta
                      firstPlayerChoice:
                        type: string
                        example: rock
                      secondPlayerChoice:
                        type: string
                        example: paper
                      result:
                        type: string
                        example: beta WIN
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: You cannot choose other choice again      
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
      
  /login:
    post:
      tags:
        - general
      summary: login
      requestBody:
        description: login
        content:
          application/json:
            schema:
              type: object
              properties:
                userName:
                  type: string
                  example: alpha
                password:
                  type: string
                  example: password
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  token:
                    type: string
                    example: token
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: array
                    items:
                      type: string
                      example: Invalid value for password
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: Unauthorized
                    
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT