function createNode(element) {
  return document.createElement(element);
}

function append(parrent, el) {
  return parrent.appendChild(el);
}

function createTableDataWithText(
  tr,
  text,
  isUrl = false,
  options = { action: 'delete' }
) {
  let td = createNode('td');
  let textToAppen = document.createTextNode(text);

  if (isUrl) {
    let a = createNode('a');
    if (options.action === 'delete') {
      a.href = `javascript:deleteData('${options.id}', '${options.table}');`;
    } else {
      a.href = `javascript:changeRole('${options.id}', ${text});`;
    }

    append(a, textToAppen);
    append(td, a);
  } else {
    append(td, textToAppen);
  }

  append(tr, td);
}

function deleteData(id, table) {
  return fetch(`/apis/${table}/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  }).then(async (response) => {
    if (response.ok) {
      if (table === 'users') {
        return createTableUsers();
      } else {
        return createTableManageRooms();
      }
    }

    const message = await response.json();
    return alert(message.message);
  });
}

function changeRole(id, isAdmin) {
  let isConfirm = confirm('Are you sure want to change user role?');

  if (isConfirm) {
    const data = { isAdmin: true };

    if (isAdmin) {
      data.isAdmin = false;
    }

    return fetch(`/apis/users/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then(async (response) => {
      if (response.ok) {
        return createTableUsers();
      }

      const message = await response.json();
      return alert(message.message);
    });
  }
}

function createTableUsers() {
  async function getUsersData() {
    const myHeaders = new Headers();
    myHeaders.append(
      'Cookie',
      'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImM0Y2IzNTNiLTBmMWUtNDU1Yi1iNjM0LTU2MmE2ZDM5YmM5MCIsInVzZXJOYW1lIjoiYWxwaGEiLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2MDc5MDM1NzUsImV4cCI6MTYwNzkwNzE3NX0.cNklyda4OvE0OBh9yHWAA8pMWFFeA0_8R1quIZJxjws'
    );

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',
    };

    const result = await fetch('/apis/users', requestOptions);
    return result.json();
  }

  async function renderTable01() {
    const tableBody01 = document.getElementById('tableBody01');
    if (tableBody01) {
      tableBody01.innerHTML = '';
    }

    const data = await getUsersData();
    const usersData = data.message;

    if (usersData.length > 0) {
      usersData.forEach((user) => {
        const tr = createNode('tr');

        createTableDataWithText(tr, user.userName);
        createTableDataWithText(tr, user.profile.name);
        createTableDataWithText(tr, user.profile.email);
        createTableDataWithText(tr, user.isAdmin, true, {
          id: user.id,
          action: 'update',
        });
        createTableDataWithText(tr, '🗑️', true, {
          id: user.id,
          table: 'users',
          action: 'delete',
        });

        append(tableBody01, tr);
      });
    }
  }

  renderTable01();
}
createTableUsers();

function createTableManageRooms() {
  async function getRoomsData() {
    const myHeaders = new Headers();
    myHeaders.append(
      'Cookie',
      'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImM0Y2IzNTNiLTBmMWUtNDU1Yi1iNjM0LTU2MmE2ZDM5YmM5MCIsInVzZXJOYW1lIjoiYWxwaGEiLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2MDc5MDkxMDIsImV4cCI6MTYwNzkxMjcwMn0.gjYnPEyz7oHoiFJWgA2hFRxU_5t3OYosinO71bIT9mc'
    );

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',
    };

    const result = await fetch('/apis/rooms', requestOptions);
    return await result.json();
  }

  async function renderTable02() {
    const tableBody02 = document.getElementById('tableBody02');
    if (tableBody02) {
      tableBody02.innerHTML = '';
    }

    const data = await getRoomsData();
    const roomsData = data.message;

    if (roomsData.length > 0) {
      roomsData.forEach((room) => {
        const tr = createNode('tr');
        console.log(room);

        createTableDataWithText(tr, room.id.slice(0, 5));
        createTableDataWithText(tr, room.firstPlayer);
        createTableDataWithText(tr, room.secondPlayer);
        createTableDataWithText(tr, room.firstPlayerChoice);
        createTableDataWithText(tr, room.secondPlayerChoice);
        createTableDataWithText(tr, room.result);
        createTableDataWithText(tr, '🗑️', true, {
          id: room.id,
          table: 'rooms',
          action: 'delete',
        });

        append(tableBody02, tr);
      });
    }
  }

  renderTable02();
}
createTableManageRooms();

const logout = () => {
  document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
  window.location.href = '/';
};
