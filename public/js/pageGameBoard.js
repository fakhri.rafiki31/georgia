function getCookie(cname) {
  const name = `${cname}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    // eslint-disable-next-line eqeqeq
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    // eslint-disable-next-line eqeqeq
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

function createRoom() {
  const messageContainer = document.getElementById('gameMessageContainer');
  const userName = 'alpha';

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const raw = JSON.stringify({ userName });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('/apis/rooms', requestOptions)
    .then((response) => response.json())
    .then((result) => {
      if (result.message === 'Internal Server Error') {
        messageContainer.innerHTML = result.message;
        messageContainer.style.display = 'block';

        return false;
      }

      document.cookie = `roomId=${result.roomId}`;
      console.log(result);
      window.location.href = '/gameboard';
      return true;
    })
    .catch((error) => console.log('error', error));
}

async function joinRooms() {
  const roomId = document.getElementById('id03RoomId').value;
  const userPlayer = document.getElementById('userProfilNameNav').innerText;
  const messageContainer = document.getElementById('id03Message');
  console.log(roomId, userPlayer);

  //   const userData = await getUserDataByToken();
  //   console.log(userData);

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const raw = JSON.stringify({ userName: userPlayer });

  const requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch(`/apis/rooms/${roomId}`, requestOptions)
    .then(async (response) => {
      if (response.ok){
        return response.json()
      }

      const msg = await response.json()
      throw Error(msg.message.toString())
    })
    .then((result) => {
      console.log(result);
      document.cookie = `roomId=${roomId}`;
      window.location.href = '/gameboard';
      return true;
    })
    .catch((error) => {
      messageContainer.innerHTML = error.message;
      messageContainer.style.display = 'block';
    });
}

async function getUserDataByToken() {
  const TOKEN_LOGIN = 'token';
  const loginToken = getCookie(TOKEN_LOGIN);
  const myHeaders = new Headers();
  myHeaders.append('Cookie', `token=${loginToken}`);

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const response = await fetch('/apis/userprofiles', requestOptions);

  if (!response) {
    const message = `An error has occured: ${response.status}`;
    throw new Error(message);
  }

  const userData = await response.json();
  return userData;
}

async function getRoomGameData() {
  const ROOM_COOKIE = 'roomId';
  const roomId = getCookie(ROOM_COOKIE);

  const TOKEN_LOGIN = 'token';
  const loginToken = `token=${getCookie(TOKEN_LOGIN)}`;

  const myHeaders = new Headers();
  myHeaders.append('Cookie', loginToken);

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const response = await fetch(`/apis/games/${roomId}`, requestOptions);
  const gameData = await response.json();
  return gameData;
}

const refreshButtonHandler = async () => {
  const gameBoardInfo = document.getElementById('gameBoardInfo');
  const gameBoardResult = document.getElementById('modalInfoResult');
  const response = await getRoomGameData();

  //   console.log(response.room);

  if (!response.room.result) return;
  gameBoardInfo.innerHTML = response.room.result;

  if (response.room.firstPlayerChoice && response.room.secondPlayerChoice) {
    gameBoardResult.innerHTML = response.room.result;
    document.getElementById('id05').style.display = 'flex';
  }
};

refreshButtonHandler();

const copyRoomIdButton = async () => {
  const copyGameIdButton = document.getElementById('copyGameIdButton');
  const response = await getRoomGameData();
  const roomId = response.room.id;
  copyGameIdButton.setAttribute('data-room', roomId);
};

const gameInit = async () => {
  const playerOption = document.getElementById('rockPaperScissors');
  const copyGameIdButton = document.getElementById('copyGameIdButton');
  const response = await getRoomGameData();
  const roomId = response.room.id;
  copyGameIdButton.setAttribute('data-room', roomId);
  copyGameIdButton.addEventListener('click', () => {
    alert(copyGameIdButton.dataset.room);
  });

  let playerHero;
  if (playerOption) {
    playerOption.addEventListener('click', (e) => {
      playerHero = e.target.dataset.hero;
      playerChooseHandler(playerHero);
      refreshButtonHandler();
    });
  }
};

gameInit();

async function playerChooseHandler(playerHero) {
  const ROOM_COOKIE = 'roomId';
  const roomId = getCookie(ROOM_COOKIE);
  const TOKEN_LOGIN = 'token';
  const loginToken = getCookie(TOKEN_LOGIN);
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  myHeaders.append('Cookie', `token=${loginToken}`);

  const raw = JSON.stringify({ choice: playerHero });

  const requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch(`/apis/games/${roomId}`, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      if (result.message) return alert(result.message);
      //   console.log(result);

      //   document.getElementById('id05').style.display = 'flex';
    })
    .catch((error) => console.log('error', error));
}
