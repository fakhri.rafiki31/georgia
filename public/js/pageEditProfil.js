// UNTUK HALAMAN EDIT PROFIL

function getCookie(cname) {
  const name = `${cname}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    // eslint-disable-next-line eqeqeq
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    // eslint-disable-next-line eqeqeq
    if (c.indexOf(name) == 0) {
      return `${cname}=${c.substring(name.length, c.length)}`;
    }
  }
  return '';
}

async function getUserDataByToken() {
  const TOKEN_LOGIN = 'token';
  const loginToken = getCookie(TOKEN_LOGIN);
  const myHeaders = new Headers();
  myHeaders.append('Cookie', loginToken);

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const response = await fetch('/apis/userprofiles', requestOptions);

  if (!response) {
    const message = `An error has occured: ${response.status}`;
    throw new Error(message);
  }

  const userData = await response.json();
  return userData;
}

async function editProfilInitPage() {
  const nameInput = document.getElementById('editProfilNameInput');
  const emailInput = document.getElementById('editProfilEmailInput');

  const userData = await getUserDataByToken();
  //   console.log(userData);
  nameInput.value = userData.profile.name;
  emailInput.value = userData.profile.email;
}
editProfilInitPage();

async function editProfilInitHandler() {
  const newName = document.getElementById('editProfilNameInput').value;
  const newEmail = document.getElementById('editProfilEmailInput').value;

  const userData = await getUserDataByToken();
  userID = userData.profile.id;
  console.log(newName);
  console.log(newEmail);
  console.log(userID);

  const TOKEN_LOGIN = 'token';
  const loginToken = getCookie(TOKEN_LOGIN);
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  myHeaders.append('Cookie', loginToken);

  const raw = JSON.stringify({
    name: newName,
    email: newEmail,
  });

  const requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch(`/apis/userprofiles/${userID}`, requestOptions)
    .then((response) => response.json())
    .then((result) => alert(result.message))
    .catch((error) => console.log('error', error));
}
