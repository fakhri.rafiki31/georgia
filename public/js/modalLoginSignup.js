navSlide = () => {
  const burger = document.querySelector('.header__burger');
  const menu = document.querySelector('.header__menu');

  document.addEventListener('scroll', () => {
    menu.classList.remove('active');
    burger.classList.remove('toggle');
  });

  burger.addEventListener('click', () => {
    menu.classList.toggle('active');

    burger.classList.toggle('toggle');
  });
};

navSlide();

modal = () => {
  const modal = document.querySelectorAll('.modal');
  modal.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      if (e.target !== btn) return;

      // eslint-disable-next-line no-param-reassign
      btn.style.display = 'none';
    });
  });
};

modal();

login = () => {
  const button = document.getElementById('spanLogin');
  const login = document.getElementById('id02');
  const signup = document.getElementById('id01');

  button.addEventListener('click', (e) => {
    e.preventDefault();

    signup.style.display = 'none';
    login.style.display = 'flex';
  });
};

signup = () => {
  const button = document.getElementById('spanSignup');
  const login = document.getElementById('id02');
  const signup = document.getElementById('id01');

  button.addEventListener('click', (e) => {
    e.preventDefault();

    login.style.display = 'none';
    signup.style.display = 'flex';
  });
};

joinRoom = () => {
  const button = document.getElementById('spanSignup');
  // const login = document.getElementById('id02')
  // const signup = document.getElementById('id01')
  const joinRoom = document.getElementById('id03');

  button.addEventListener('click', (e) => {
    e.preventDefault();

    login.style.display = 'none';
    signup.style.display = 'flex';
  });
};

const btn = document.getElementById('id01Submit');
btn.addEventListener('click', (e) => {
  e.preventDefault();
});

const register = () => {
  const name = document.getElementById('id01Name').value;
  const username = document.getElementById('id01Username').value;
  const email = document.getElementById('id01Email').value;
  const password = document.getElementById('id01Password').value;
  const passwordRepeat = document.getElementById('id01PasswordRepeat').value;

  const messageContainer = document.getElementById('id01Message');

  if (!(name && username && email && password)) {
    messageContainer.innerHTML = 'all fields are required';
    messageContainer.style.display = 'block';
    return;
  }

  if (password !== passwordRepeat) {
    messageContainer.innerHTML = 'password not match';
    messageContainer.style.display = 'block';
    return;
  }

  const url = 'apis/users';
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const raw = JSON.stringify({
    name,
    userName: username,
    email,
    password,
  });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch(url, requestOptions)
    .then(async (response) => {
      if(response.ok){
        document.getElementById('id01').style.display = 'none';

        setTimeout(() => {
          document.getElementById('id04').style.display = 'flex';
        }, 500);
      }

      const msg = await response.json()
      throw Error(msg.message.toString())
    })
    .catch((error) => {
      messageContainer.innerHTML = error.message;
      messageContainer.style.display = 'block';
    });
};

function loginFetch() {
  const userName = document.getElementById('id02Username').value;
  const password = document.getElementById('id02Password').value;

  const messageContainer = document.getElementById('id02Message');

  if (!(userName && password)) {
    messageContainer.innerHTML = 'all fields are required';
    messageContainer.style.display = 'block';
    return;
  }

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const raw = JSON.stringify({ userName, password });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
  };

  fetch('/apis/login', requestOptions)
    .then(async (response) => {
      if(response.ok){
        return response.json()
      }

      const msg = await response.json()
      throw Error(msg.message.toString())
    })
    .then((result) => {
      // localStorage.setItem('token', result.token)
      window.location.href = '/game';
      return true;
      // console.log(document.cookie)
    })
    .catch((error) => {
      if (error.message === 'Unauthorized') {
        messageContainer.innerHTML = 'username or password did not match';
      } else {
        messageContainer.innerHTML = error.message;
      }

      messageContainer.style.display = 'block';

      return false;
    });

  return false;
}

const dropdown = () => {
  const profilLogo = document.querySelector('.userlogo--login');
  const dropdownMenu = document.getElementById('header__dropdown');

  profilLogo.addEventListener('click', (e) => {
    dropdownMenu.classList.toggle('displayToggle');
  });
};
dropdown();

const displayLoginSignUpList = () => {
  document.getElementById('loginNavHeader').style.display = 'block';
  document.getElementById('signupNavHeader').style.display = 'block';
  document.getElementById('logoNavHeader').style.display = 'none';
};

const displayUserLogoList = () => {
  document.getElementById('loginNavHeader').style.display = 'none';
  document.getElementById('signupNavHeader').style.display = 'none';
  document.getElementById('logoNavHeader').style.display = 'block';
};

const changeNavBarListItemToUserLogo = () => {
  // eslint-disable-next-line no-use-before-define
  const isLogin = check_cookie_name('token');
  if (isLogin) return displayUserLogoList();
  return displayLoginSignUpList();
};
changeNavBarListItemToUserLogo();

const logout = () => {
  document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
  window.location.href = '/';
};

// eslint-disable-next-line camelcase
function check_cookie_name(name) {
  const match = document.cookie.match(new RegExp(`(^| )${name}=([^;]+)`));
  if (match) {
    return true;
  }

  return false;
}
